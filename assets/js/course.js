// /course.html?courseId=60236f7fa57a8724b07d0457
//URL Query parameters
/*
	?courseId=60236f7fa57a8724b07d0457

	? - start of query string
	courseId= (parameter name)
	60236f7fa57a8724b07d0457 = value

*/
//window.location.search return the query string in the URL
//console.log(window.location.search)

//instatiate or create a new URLSearchParams object.
//This object, URLSearchParams, is used an interface to gain access to methods that allow us to specific parts of the query string.
let params = new URLSearchParams(window.location.search)

//The has method for URLSearchParams checks if the courseId key exists in our URL Query string.
//console.log(params.has('courseId'))//true, if the there is a key

//The get method for URLSearchParams returns the value of the key passed in as an argument.
//console.log(params.get('courseId'))

//store the courseID from the URL Query string in a variable:
let courseId = params.get('courseId')

//get the token from localStorage
let token = localStorage.getItem('token')


let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

//enroll is not defined:
//not defined vs undefined?
/*
	undefined = variable was declared without initial value.
	not defined = the variable does not exist.
*/
//Get the details of a single course.
// api/courses/:id
fetch(`http://localhost:8000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

	document.querySelector("#enrollButton").addEventListener("click", ()=>{

		//add fetch request to enroll our user:
		fetch('http://localhost:8000/api/users/enroll', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})

		})
		.then(res => res.json())
		.then(data => {
			//redirect the user to the courses page after enrolling.
			if(data === true){
				//show an alert to thank the user:
				alert('Thank you for enrolling to the course.')
				window.location.replace('./courses.html')
			} else {

				//server error while enrolling to course:
				alert("Something Went Wrong")

			}
		})

		/*Resume at 1:10 PM with Sir Arvin*/
	})
})