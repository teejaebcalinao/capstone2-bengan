let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let cardFooter;

//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.
if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {
	addButton.innerHTML = `

		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>


	`
}

fetch('http://localhost:8000/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// A variable that will store the data to be rendered
	let courseData;

	// If the number of courses is less than 1, display no courses available
	if(data.length < 1) {
		courseData = "No courses available";
	} else {

		courseData = data.map(course => {

			console.log(course);

			if(adminUser == false || !adminUser) {
				cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`
			} else {
				cardFooter =
				`
					<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
						Edit
					</a>
					<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton">
						Delete
					</a>
				`
			}

			return(
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// since the collection is an array, we can use the join method to indicate the separator for each element
		}).join("")

	}

	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData;

})

/*
	Activity:

	use the fetch Method to get all the courses and show the data into the console using console.log()

	If you are done, create a new folder (s19) in gitlab
	inside s19 folder, create a new repo: d1

	In your local machine, connect your d1 to your online repo:
	git remote add origin <url>

	Then add, commit and push it into your new repo.

	Link to boodle as:
	WD057-11 | Express.js - Booking System API Integration


*/

/*<div id="footer">
	<h1>Hello</h1>
</div>

document.querySelector("#footer").innerHTML("<h1>Hello</h1>")*/

/*course = [

<div class="col-md-6 my-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">${course.name}</h5>
			<p class="card-text text-left">
				${course.description}
			</p>
			<p class="card-text text-right">
				₱ ${course.price}
			</p>
		</div>
		<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>

,

<div class="col-md-6 my-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">${course.name}</h5>
			<p class="card-text text-left">
				${course.description}
			</p>
			<p class="card-text text-right">
				₱ ${course.price}
			</p>
		</div>
		<div class="card-footer">
			${cardFooter}
		</div>
	</div>
</div>

]*/